"""
 * A. Tangherloni et al.: "scAEspy: a unifying tool based on
   autoencoders for the analysis of single-cell RNA sequencing data",
   bioRxiv, 727867, 2018. doi: 10.1101/727867.

 * Copyright (C) 2019 - Andrea Tangherloni
 * Distributed under the terms of the GNU General Public License (GPL)
 * This file is part of MedGA.

 * scAEspy is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License v3.0 as published by
 * the Free Software Foundation.
  
 * scAEspy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
"""



import argparse, os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from core import scAEspy

def getArgs():

    parser = argparse.ArgumentParser(add_help=False)
    
    # The required setting
    parser.add_argument('--matrix',      help='Gene expression matrix (each row must be a cell)', required=True)

    # Optional settings
    parser.add_argument('--output',      help='Output folder', nargs='?', default="output", type=str)
    parser.add_argument('--latent',      help='Dimensions of the latent space', nargs='?', default=16, type=int)
    parser.add_argument('--hidden',      help='Dimensions of the hidden layers (list)', nargs='+', default=[64], type=int)
    parser.add_argument('--loss',        help='Loss function', nargs='?', default="Poisson", type=str)
    parser.add_argument('--activation',  help='Activation fuction', nargs='?', default="sigmoid", type=str)
    parser.add_argument('--epochs',      help='Number of epochs', nargs='?', default=100, type=int)
    parser.add_argument('--batch',       help='Batch size', nargs='?', default=100, type=int)
    parser.add_argument('--gaussian',    help='Number of Gaussian distribution(s)', nargs='?', default=1, type=int)
    parser.add_argument('--alpha',       help='alpha setting used to balance between KL and MMD', nargs='?', default=0,  type=int)
    parser.add_argument('--lambda',      help='lambda setting used to balance between KL and MMD', nargs='?', default=1, type=int)
    parser.add_argument('--patience',    help='Max patience', nargs='?', default=None, type=int)
    parser.add_argument('--seed',        help='Seed value used for reproducibility', nargs='?', default=None, type=int)
    parser.add_argument('--synthetic',   help='Number of synthetic cells to generate', nargs='?', default=None, type=int)

    # Settings without a parameter value
    parser.add_argument('--constrained', help='Enable the constrained version of the loss fuction', action='store_true')
    parser.add_argument('--clipping',    help='Clip the value when NB loss fuctions are used', action='store_true')
    parser.add_argument('--prior',       help='Enable the learnable prior distribution', action='store_true')
    parser.add_argument('--split',       help='Split the provided matrix into train and test sets', action='store_true')
    parser.add_argument('--plot',        help='Plot the term of the ELBO function', action='store_true')
    parser.add_argument('--verbose',     help='Enable the verbose modality of scAEspy', action='store_true')
    parser.add_argument('--help',        action='help')

    args = vars(parser.parse_args())

    return args

def prefixAE(args):
    if args["alpha"] == 0 and args["lambda"] == 1:
        if args["gaussian"] == 1:
            value =  "VAE"
        elif args["gaussian"] > 1:
            value =  "GMVAE"

    elif args["alpha"] == 1 and args["lambda"] == 1:
        if args["gaussian"] == 1:
            value =  "MMDVAE"
        elif args["gaussian"] > 1:
            value =  "GMMMD"

    elif args["alpha"] == 0 and args["lambda"] == 2:
        value =  "GMMMDVAE"

    else:
        value = "AE_alpha=%d_lambda=%d"%(args["alpha"], args["lambda"])

    return value

def saveCells(args, prefix, synthetic_cells, reconstructed_cells, latent_cells):
    
    if args["synthetic"] is not None:
        np.savetxt(args["output"]+os.sep+"%s_synthetic_cells.tsv"%prefix, synthetic_cells, fmt='%.5f', delimiter='\t')
    
    np.savetxt(args["output"]+os.sep+"%s_reconstructed_cells.tsv"%prefix, reconstructed_cells, fmt='%.5f', delimiter='\t')
    np.savetxt(args["output"]+os.sep+"%s_latent_representation.tsv"%prefix, latent_cells, fmt='%.5f', delimiter='\t')

def saveLosses(args, prefix, history):

    np.savetxt(args["output"]+os.sep+"%s_trainKLy.tsv"%prefix, np.transpose(history["train_kly"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_testKLy.tsv"%prefix,  np.transpose(history["test_kly"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_trainKLz.tsv"%prefix, np.transpose(history["train_klz"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_testKLz.tsv"%prefix,  np.transpose(history["test_klz"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_trainMMD.tsv"%prefix, np.transpose(history["train_mmd"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_testMMD.tsv"%prefix,  np.transpose(history["test_mmd"]), fmt='%.5f', delimiter='\n')

    np.savetxt(args["output"]+os.sep+"%s_trainReconstruction.tsv"%prefix, np.transpose(history["train_rec"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_testReconstruction.tsv"%prefix,  np.transpose(history["test_rec"]), fmt='%.5f', delimiter='\n')

    np.savetxt(args["output"]+os.sep+"%s_trainLOSS.tsv"%prefix, np.transpose(history["train_loss"]), fmt='%.5f', delimiter='\n')
    np.savetxt(args["output"]+os.sep+"%s_testLOSS.tsv"%prefix,  np.transpose(history["test_loss"]), fmt='%.5f', delimiter='\n')


if __name__ == '__main__':

    print("*"*100)
    print("* scAEspy: a unifying tool based on autoencoders for the analysis of single-cell RNA sequencing data\n")

    args = getArgs() 

    try:
        matrix = np.loadtxt(args["matrix"])
    except:
        print("Error, unable to load the provided gene expression matrix")
        exit(-100)

    prefix = prefixAE(args)

    if args["verbose"]:
        print("* Initialising scAEspy ...")
        print("\t* alpha = %2d; lambda = %2d -> %s"%(args["alpha"], args["lambda"], prefix))
        print("\t* Loss                    -> %s"%args["loss"])
        print("\t* Constrained             -> %s"%args["constrained"])
        print("\t* Number of Gaussian(s)   -> %s"%args["gaussian"])
        print("\t* Learnable prior         -> %s"%args["prior"])
        print("\t* Number of epochs        -> %s"%args["epochs"])
        print("\t* Batch size              -> %s"%args["batch"])
        print("\t* Max patience (epochs)   -> %s"%args["patience"])
        print("\n")

    scaespy = scAEspy(matrix.shape[1],
                      hidden_layers   = args["hidden"],
                      latent_layer    = args["latent"],
                      activation      = args["activation"],
                      rec_loss        = args["loss"],
                      num_gaussians   = args["gaussian"],
                      learnable_prior = args["prior"],
                      alpha           = args["alpha"],
                      lambd           = args["lambda"],
                      constrained     = args["constrained"],
                      clipping        = args["clipping"],
                      verbose         = args["verbose"],
                      seed            = args["seed"])

    if args["verbose"]:
        print("\n* Building %s ..."%prefix)
        print("*"*100)
    
    scaespy.build()

    if args["split"]:
        if args["verbose"]:
            print("* Splitting in training and validation sets shuffling the data ...")
    
        x_train, x_test = train_test_split(matrix, test_size=0.1, random_state=42, shuffle=True)
        scaespy.train(x_train, x_test, epochs=args["epochs"], batch_size=args["batch"], max_patience=args["patience"])
    
    else:
        if args["verbose"]:
            print("*  Shuffling the data ...")
        
        matrix_shuffled = shuffle(matrix, random_state=42)
        scaespy.train(matrix_shuffled, matrix_shuffled, epochs=args["epochs"], batch_size=args["batch"], max_patience=args["patience"])

    if not os.path.isdir(args["output"]):
        os.mkdir(args["output"])

    history = scaespy.getHistory()
    if args["verbose"]:
        print("*"*100)
        print("*  Saving the values of the terms of the ELBO function ...")
    saveLosses(args, prefix, history)

    if args["plot"]:
        if args["verbose"]:
            print("*  Plotting the terms of the ELBO function ...")
        scaespy.plotLosses(show=False, folder=args["output"], name=prefix)

    synthetic_cells     = None
    if args["verbose"]:
        print("*  Generating the reconstructed cells ...")
    reconstructed_cells = scaespy.reconstructedRepresentation(matrix)

    if args["verbose"]:
        print("*  Generating the latent representation of the cells ...")
    latent_cells        = scaespy.latentRepresentation(matrix)

    if args["synthetic"] is not None:
        if args["verbose"]:
            print("*  Generating %d synthetic cells ..."%args["synthetic"])
        synthetic_cells = scaespy.sampling(args["synthetic"])

    if args["verbose"]:
        print("*  Saving the generated data ...")
    saveCells(args, prefix, synthetic_cells, reconstructed_cells, latent_cells)
    
    print("*"*100)
    print("\n")
