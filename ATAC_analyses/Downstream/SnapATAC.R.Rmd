---
title: "SnapATAC_merged"
output: html_notebook
---

```{r}
library(SnapATAC)
library(viridisLite);
library(ggplot2);
```

```{r}
x.sp = createSnap(
    file="merged.snap",
    sample="merged234",
    num.cores=4
  );
# at this stage might be good to remove cells with low quality 
```

```{r}
showBinSizes("merged.snap");


x.sp = addBmatToSnap(x.sp, bin.size=5000, num.cores=4);

```

```{r}
x.sp = makeBinary(x.sp, mat="bmat")
```

```{r}
library(GenomicRanges);
black_list = read.table("hg38_blacklist.bed");
black_list.gr = GRanges(
    black_list[,1], 
    IRanges(black_list[,2], black_list[,3])
  );
idy = queryHits(findOverlaps(x.sp@feature, black_list.gr));
if(length(idy) > 0){x.sp = x.sp[,-idy, mat="bmat"]};
x.sp
```

```{r}
chr.exclude = seqlevels(x.sp@feature)[grep("random|chrM", seqlevels(x.sp@feature))];
idy = grep(paste(chr.exclude, collapse="|"), x.sp@feature);
if(length(idy) > 0){x.sp = x.sp[,-idy, mat="bmat"]};
 x.sp
```

```{r}
bin.cov = log10(Matrix::colSums(x.sp@bmat)+1);
hist(
    bin.cov[bin.cov > 0], 
    xlab="log10(bin cov)", 
    main="log10(Bin Cov)", 
    col="lightblue", 
    xlim=c(0, 5)
  );
 bin.cutoff = quantile(bin.cov[bin.cov > 0], 0.95);
 idy = which(bin.cov <= bin.cutoff & bin.cov > 0);
 x.sp = x.sp[, idy, mat="bmat"];
 x.sp
```

```{r}
x.sp = runDiffusionMaps(
    obj=x.sp,
    input.mat="bmat", 
    num.eigs=50
  );
```


```{r}
plotDimReductPW(
    obj=x.sp, 
    eigs.dims=1:50,
    point.size=0.3,
    point.color="grey",
    point.shape=19,
    point.alpha=0.6,
    down.sample=5000,
    pdf.file.name=NULL, 
    pdf.height=7, 
    pdf.width=7
  );

# x.sp@metaData = cbind(x.sp@metaData, coldata.final)
```


```{r}
x.sp = runKNN(
    obj=x.after.sp,
    eigs.dims= 1:50,
    k=10
  );

x.sprunCluster(
    obj=x.after.sp,
    tmp.folder=tempdir(),
    louvain.lib="R-igraph",
    seed.use=10
  );

x.sp@metaData$cluster = x.sp@cluster;
```


```{r}
x.sp = runViz(
    obj=x.sp, 
    tmp.folder=tempdir(),
    dims=2,
    eigs.dims=2:50, 
    method="umap",
    seed.use=10
  );

 plotViz(
    obj=x.sp,
    method="umap",
    point.color=x.sp@sample, 
    point.size=0.1, 
    text.add= FALSE,
    down.sample=10000,
    legend.add=TRUE
  );


```


```{r}
# calculate the ensemble signals for each cluster
ensemble.ls = lapply(split(seq(length(x.sp@cluster)), x.sp@cluster), function(x){
	SnapATAC::colMeans(x.sp[x,], mat="bmat");
	})
# cluster using 1-cor as distance  
hc = hclust(as.dist(1 - cor(t(do.call(rbind, ensemble.ls)))), method="ward.D2");
plotViz(
    obj=x.sp,
    method="umap", 
    main="batch1",
    point.color=x.sp@cluster, 
    point.size=1, 
    point.shape=19, 
    point.alpha=0.8, 
    text.add=TRUE,
    text.size=1.5,
    text.color="black",
    text.halo.add=TRUE,
    text.halo.color="white",
    text.halo.width=0.2,
    down.sample=10000,
    legend.add=FALSE
    );
plot(hc, hang=-1, xlab="");
```

```{r}
clusters.sel = names(table(x.sp@cluster))[which(table(x.sp@cluster) > 50)];
peaks.ls = mclapply(seq(clusters.sel), function(i){
    print(clusters.sel[i]);
    runMACS(
        obj=x.sp[which(x.sp@cluster==clusters.sel[i]),], 
        output.prefix=paste0("atac_v1_adult_brain_fresh_5k.", gsub(" ", "_", clusters.sel)[i]),
        path.to.snaptools="/home/berest/miniconda2/bin/snaptools",
        path.to.macs="/home/berest/miniconda2/bin/macs2",
        gsize="hs", # mm, hs, etc
        buffer.size=500, 
        num.cores=1,
        macs.options="--nomodel --shift 100 --ext 200 --qval 5e-2 -B ",
        tmp.folder=tempdir()
   );
 }, mc.cores=5);
# assuming all .narrowPeak files in the current folder are generated from the clusters
peaks.names = system("ls | grep narrowPeak", intern=TRUE);
peak.gr.ls = lapply(peaks.names, function(x){
    peak.df = read.table(x)
    GRanges(peak.df[,1], IRanges(peak.df[,2], peak.df[,3]))
  })
peak.gr = GenomicRanges::reduce(Reduce(c, peak.gr.ls));
peak.gr
peak.df = as.data.frame(peak.gr)
```

```{r}
peak.df$seqnames =  substr(substring(peak.df$seqnames,3),1,nchar(substring(peak.df$seqnames,3))-1) 

allowchr = c(paste0("chr",seq(1:23)),"chrY","chrX")
peak.filt.df = peak.df[which(peak.df$seqnames %in% allowchr),]

library(tidyverse)
write_tsv(peak.filt.df, path = "snapatac.clust.peaks.bed",col_names = F)
```


